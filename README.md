# amchartsapp

## Sobre o Projeto

O projeto visa apresentar o recurso de Lazy Load do [Webpack] (https://webpack.js.org/guides/lazy-loading/). Carregamento Preguiçoso ou Lazy Load, é responsável pelo carregamento assíncrono de arquivos javascript. 

Neste exemplo, utilizo o [AmCharts] (https://www.amcharts.com/) na versão 4 que já trás integração com o Vue.js. A partir do código exibido na página de [documentação] (https://www.amcharts.com/docs/v4/getting-started/integrations/using-vue-js/) utilizo da técnica de Lazy Load do Webpack para o carregamento assíncrono da lib do AmCharts.



Carregamento assíncrono dos bundles:
![Carregamento assincrono dos bundles](doc/resultado.png)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Editado por [https://stackedit.io](https://stackedit.io/)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjA0OTU4OTQsNTA0MDc3ODk5LDEyMTE1Mj
U0MzMsLTE2MTI4ODE3MzUsLTQ3MTk2OTA0OSw5NDQ5ODkzMTYs
MTQ0MTc1NjE5MywtMTI2NDU0ODk5OSwtMjA0NzI2MjMwNCwyMT
A5MDYwOTg5XX0=
-->